from baseMainApplication import *
from connectToLocalBd import *

class BaseDBLayoutApplication(BaseMainApplication):
    # підключення до локальної БД SQLite
    __localDb: LocalDB

    # Конструктор класу:
    def __init__(self, *args, **kwargs):
        BaseMainApplication.__init__(self, *args, **kwargs)

    # Ініціалізація локальної БД:
    def openLocalDB(self, filename: str):
        self.__localDb = LocalDB(filename)

    # Список проектів:
    def getProjectList(self):
        return self.__localDb.getProjectList()

    # Список проектів:
    def getDBList(self):
        return self.__localDb.getDBList()